## Introduction

This document serves as a guide on how to set up the RoboMaster server. The server may be used to
simulate robot matches in an environment similar to the real thing. Also the server is necessary for
testing the Referee Serial GUI protocol. These directions exist because there is currently no good
step by step procedure written in English that is accessible. These instructions are only applicable
if you are running the RoboMaster server on a Windows 10 machine.

## Setting up the Server

1. You need a router in order to run the server. We use a NetGear router (located in a cardboard box
   with a label written on it in the CV cabinet). The router should be pre-configured with the
   proper network settings necessary for the server to run properly. If for whatever reason the
   server has been reset to factory settings, refer to the [router
   configuration](#router-configuration) section below. Plug the router into a wall outlet and turn
   it on.
2. Download and set up MySQL.
   - Download the latest version of [MySQL](https://dev.mysql.com/downloads/mysql/). Download the
     Windows (x86, 64-bit), ZIP archive.

      <img src='uploads/sql-installer.PNG'>

   - If redirected to another page asking for you to sign in, you can select "no thanks, just start
     my download."
   - After downloading the `.zip` file, extract it and add the `bin` directory to your path
     environment variable (if you do not know how to add something to your path, see
     [Windows-Setup](Windows-Setup.md), step 4).
   - In the directory that contains the `bin` directory, add a `data` directory and add `mysql.ini`.

     <img src='uploads/mysql-dir.PNG'>
   - In the newly created `mysql.ini`, add the following:
      ```ini
      [mysql]

      default-character-set=utf8

      [mysqld]

      port=3306

      basedir=C:\Users\Matthew\Desktop\mysql-8.0.21-winx64\mysql-8.0.21-winx64

      datadir=C:\Users\Matthew\Desktop\mysql-8.0.21-winx64\mysql-8.0.21-winx64\data

      max_connections=200

      character-set-server=utf8

      default-storage-engine=INNODB
      ```
   - Open command prompt and enter the following command: `mysqld --initialize-insecure
     --user=mysql`
   - Enter `mysqld install` in command prompt.
   - To start the SQL service, in command prompt type `net start mysql`.
   - It is likely that you will want to turn off mysql boot to start automatically. If this is the
     case, view [this Stack Overflow
     post](https://stackoverflow.com/questions/10885038/stop-mysql-service-windows) for how to do
     this.
3. Download the latest version of the RoboMaster Server (the 2020 version may be found
   [here](https://www.robomaster.com/en-US/products/components/detail/2528), but make sure you have
   the latest version).
4. Turn off the WI-FI on your computer.
5. Connect the router to the computer that will run the RoboMaster server (your computer) with an
   ethernet cable. Ensure the ethernet cable is plugged into one of the four leftmost black ethernet
   ports on the router. With the ethernet plugged in you should see the following in your Ethernet
   settings page (to navigate to this page, hit the Windows button and type "Ethernet settings" and
   hit enter). Note that seeing "Unidentified network: No Internet" is expected.

   <img src='uploads/ethernet-page.PNG'>

6. Set the IP address of your computer to "192.168.1.2". To do this, while in the "Ethernet
   settings" page (see previous instruction), click on "Change adapter options" on the right hand
   side of the page. While in this page, select "Ethernet", then select "Properties".

   <img src='uploads/ethernet-adapter-options.PNG'>

   <img src='uploads/ethernet-status-page.PNG'>

   In the properties page scroll down and double click on "Internet Protocol Version 4 (TCP/IPv4)"

   <img src='uploads/ethernet-properties.PNG'>

   In the IPV4 Properties, select "Use the following IP address", and then set the IP address to
   "192.168.1.2" and the Subnet mask to "255.255.255.0" (as shown below).

   <img src='uploads/ipv4-properties.PNG'>

7. Turn off your firewall. Click the Windows button then type in "Windows Defender Firewall" and hit
   enter. From this page, select "Turn Windows Defender Firewall on or off."

   <img src='uploads/firewall-off.png'>

8. Start the MySQL server if you have not done so already. In a command prompt running as
   administrator, type `net start mysql`.
9. Start the RoboMaster server (`RoboMasterServer.exe`). When it has started select "StartAll".
10. You may now start the [RoboMaster
    client](https://www.robomaster.com/en-US/products/components/detail/2530) and log into the
    server on any computer connected through ethernet or Wi-Fi to the router (including the computer
    running the server).
11. Refer to the referee system documentation for how to connect a robot to the server. With server
    configured correctly, the client should automatically accept robots when they are connected to
    the router.

## Troubleshooting

If you run into the following issue when attempting to connect your a robot to the server, try
turning off your firewall and restarting your computer with firewalls turned off.

<img src='uploads/server-firewall-bug.png'>

## Setting up a NetGear Router

