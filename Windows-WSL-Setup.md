This guide for WSL works but has not been as rigorously tested as the Windows guide. This setup
utilizes Windows WSL, Ubuntu 18.04 LTS, thus allowing you to run Linux based unit tests. Since this
is not fully supported, you will need to manually deploy to the MCB via OpenOCD or Ozone. If you are
using this setup, check with a team lead for help with deploying.

__**Note:** this guide assumes your are cloning and working with the aruwlib repository but the
guide is generic enough to be used with [aruw-mcb](https://gitlab.com/aruw/controls/aruw-mcb),
[aruwlib-template-project](https://gitlab.com/aruw/controls/aruwlib-template-project),
[aruwlib-examples](https://gitlab.com/aruw/controls/aruwlib-examples), or
[aruw-edu](https://gitlab.com/aruw/controls/aruw-edu). To use this guide with any of these projects,
clone the respective repository instead of aruwlib.__

1. Enable WSL for Windows. The majority of this guide comes from
   [here](https://www.windowscentral.com/install-windows-subsystem-linux-windows-10).
    - Open up "Apps & Features" (search for it in the "Start" menu).<br>
    - Click on "Programs and Features."<br><br>
        <img
        src="https://www.windowscentral.com/sites/wpcentral.com/files/styles/xlarge/public/field/image/2019/12/apps-features-programsfeatures-option.jpg"
        height=300px><br>
    - Click the "Turn Windows features on or off" option from the left pane.<br><br>
        <img
        src="https://www.windowscentral.com/sites/wpcentral.com/files/styles/xlarge/public/field/image/2019/12/controlpanel-turn-windows-features-option.jpg"
        height=300px><br>
    - Scroll down and check the "Windows Subsystem for Linux" option.<br><br>
        <img
        src="https://www.windowscentral.com/sites/wpcentral.com/files/styles/xlarge/public/field/image/2019/12/enable-windows-subsystem-linux-windows-10.jpg"
        height=300px><br>
2. Install Ubuntu 20.04 from the windows store.
    - In the Start menu, search for "Windows Store."
    - Search for "Ubuntu 20.04" in the windows store search menu.
3. Once the installation is complete, Search for "Ubuntu 20.04 LTS" and hit enter. The first time
   you open up the distribution, you will need to create a new user account and password for your
   Linux distribution. The terminal will prompt you set up an account. **Don't forget the password
   you entered, you will have to use it a lot.**
4. See [here](https://docs.microsoft.com/en-us/windows/wsl/install-win10) if you are having issues
   installing WSL. Once WSL is set up, you should be able to open the "Ubuntu 20.04 LTS" terminal
   from the "Start" menu and see the below.<br>
    <img
    src="https://gitlab.com/aruw/controls/aruw-mcb/uploads/4a43e319a3d34e4d1d28c2c561e273dc/image.png"
    height=300px><br>
5. In the Ubuntu terminal, type `sudo apt-get update && apt-get upgrade`.
6. Download and Install [VSCode](https://code.visualstudio.com/download).
7. Open a new VSCode window (Search for "VSCode" in the start menu), and in the resulting VSCode
   editor type <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>X</kbd>, which will open up an extension
   manager. In the "Search Extensions in Marketplace" search bar, type "Remote - WSL", click on the
   resulting extension, and click on the "Install" button.
8. When the "Remote - WSL" extension is installed, in VSCode type
   <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>P</kbd> and type into the prompt that pops up "Remote
   Explorer: Focus on WSL Targets View" and hit enter.
9. On the side panel that pops up, next to the "Ubuntu-20.04" WSL target, click on the folder (see
   image below).<br>
    <img
    src="https://gitlab.com/aruw/controls/aruw-mcb/uploads/83a5b9ffdd6730426750d3b38d4d352d/image.png"
    height=400px><br>
10. Another VSCode window will pop up indicating that you are being connected via WSL. Once the
    connection is complete, open up the VSCode terminal (this can be accomplished via
    <kbd>Ctrl</kbd>+<kbd>~</kbd>).
11. With the Ubuntu 20.04 LTS WSL terminal open, type in the following commands:
    ```
    sudo apt-get install python3 python3-pip scons git
    sudo apt-get --no-install-recommends install doxygen
    pip3 install modm
    sudo apt-get install gcc build-essential libboost-all-dev
    pip3 install lbuild
    pip3 install pyelftools
    ```
12. Add lbuild to your path:
    - Open `~/.bashrc` with VSCode. This can be accomplished in the terminal via `code ~/.bashrc`.
    - Scroll down to the end of the file and type this:
        ```
        PATH="$HOME/.local/bin:$PATH"
        export PATH
        ```
    - Save the new updates.
13. Download the [latest
    version](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads)
    of the arm-none-eabi toolchain. You will need to do this via the Ubuntu 18.04 terminal as well.
    In particular, download "gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2". 
    - You can do the download on your windows machine, then in the Ubuntu terminal navigate to where
      the folder was downloaded using `cd`. Note that the "c" drive is located in `/mnt/c`.
    - Once you have found the folder, I recommend moving it somewhere permanent via `mv
      path/to/folder/folder-name path/to/destination`
    - Unzip the file through the Ubuntu terminal. This can be accomplished via `tar -xvf filename`,
      where `filename` is the name of the tar you downloaded.
14. Add the `/bin` folder of `gcc-arm-none-eabi-9-2020-q2-update` to your path:
    - Open `~/.bashrc` with VSCode (`code ~/.bashrc`).
    - Scroll down to the end of the file, and right above `PATH="$HOME/.local/bin:$PATH"`, type
      this:
        ```
        PATH=path/to/gcc-arm-none-eabi-9-2020-q2-update/bin:$PATH
        ```
       Where `path/to/gcc-arm-none-eabi-9-2020-q2-update` is replaced with your own path to the
       toolchain.
15. Clone this repository:
    - **Important:** In the WSL terminal you have open, you should navigate away from `~/` (your WSL
      home directory, which you can't access via Windows) to a directory that you can access through
      your Windows system.
    - Your "c" drive can be found via `cd /mnt/c`. From there, you can navigate using `cd` to a
      directory where you would like the cloned repository to be stored.
    - Once you have navigated to your desired directory, in the terminal type `git clone --recursive
      https://gitlab.com/aruw/controls/aruwlib.git`.
        - _If you forget the `--recursive`, run: `git submodule update --init --recursive`._
16. Open the `aruwlib` folder in your WSL version of VSCode. The first time you try to open this
    folder, you can start VSCode, then in the "Welcome" page, under the "Start" menu, find "Open
    folder...".
17. When you open up the folder, VSCode should prompt you to install the recommended extensions.
    Click "install recommended extensions". You should now have the "C/C++", "Cortex-Debug", and
    "aruw robot chooser" extensions.
    - _**Note:** The Cortex-Debug extension must be installed on your Windows version of VSCode (its
      OK if it is default installed on your WSL version of VSCode). To do this, open up a new VSCode
      terminal, open up the extension marketplace <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>X</kbd>) and
      into the search menu type "Cortex-Debug". Install the extension that comes up._
18. The next steps are not to be done through WSL (do not use the Ubuntu 18.04 LTS terminal you have
    open).
19. Download the latest version of [openocd](https://gnutoolchains.com/arm-eabi/openocd/). Download
    the newest version of the software and extract the files to a permanent location. The downloaded
    file is of type `.7zip`, so you will need a way to extract 7zip files, either by installing the
    7zip extractor or by using an online unzipper.
20. Add the path of the `/bin` directory of OpenOCD to your PATH environment variable.
    - In your Start menu, search for "environment" and select "Edit the system environment
      variables."
    - In the "User variables for _{your username here}_", where _{your username here}_ is a
      placeholder for your actual name. Double click on "Path."<br><br>
      <img
      src="https://gitlab.com/aruw/controls/aruw-mcb/uploads/de0e4abb49593a7c8eec7bee2b84fcaa/image.jpg"
      height="500px"><br>
    - In "Edit environment variable," click "New."<br><br>
      <img
      src="https://gitlab.com/aruw/controls/aruw-mcb/uploads/999d7561f2b8b0726bcd36979b6ac04a/image.jpg"
      height="500px"><br>
    - Add the absolute path of the `/bin` directory of OpenOCD to the box that shows up after you
      click "New". <br><br>
      <img
      src="https://gitlab.com/aruw/controls/aruw-mcb/uploads/2e5418bbe67e7646356890eba94276fc/image.jpg"
      height="500px"><br>
    - Select "OK."
21. Download the ST-Link V-2 driver
    [here](https://drive.google.com/drive/u/1/folders/1Ndk8Q-uUtzo3sQtzOoguDAVBZSM3IKT1), unzip, and
    run the executable to install the driver.
22. Download
    [ozone_jlink_jscope.zip](https://drive.google.com/drive/u/1/folders/1E9i1JBILotoFClKc6L3-m4OWuW0kiO5A)
    in our google drive. This contains installers for Ozone, the J-Link drivers, and J-Scope (all
    J-Link related software). Once downloaded and unzipped, run the three installers. 
23. Restart your computer to ensure everything has been installed properly.
