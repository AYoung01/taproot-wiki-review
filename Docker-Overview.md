We develop code for our robot in a Docker container. You may be wondering what Docker is, so this
page is dedicated to providing some information about Docker and some context into why we chose to
use it in the first place.

## What is a Docker container?

A Docker container is "a lightweight, standalone, executable package of software that includes
everything needed to run an application: code, runtime, system tools, system libraries and
settings." [[1]](https://www.docker.com/resources/what-container). A Docker container runs on the
Docker Engine, which may run on Windows, Linux, or Mac.

## Why do we use Docker?

A Docker container provides an isolated environment with which to develop software, which means the
development environment is consistent even when running on different host operating systems. Since
the Docker image contains all packages and libraries necessary to run our code, you also do not have
to go through the arduous process of installing many packages and configuring your system properly
to be able to build and run code.

Another reason why we choose to use Docker is that our code must run in a Linux environment, and
container provides such an environment. This applies even if the host machine (i.e. the machine on
which the container is installed) is not Linux based.

## Where is my Docker container stored?

When using Docker, the question of where the Docker container itself is stored comes up. You may
also wonder where the files in the Docker container actually are, since if you attempt to find them
on your machine, you will not be successful.

First off, the Docker container and any files inside of it, including any files you write or any
cloned repositories that are contained within it _are_ on your computer. Second, the location of a
Docker image varies from operating system to operating system. Whether the Docker image runs a Linux
environment or not also affects where the image is located. If you are specifically running Docker
Desktop on Windows and your container is Linux based (as is the container that code from `aruwlib`
is run in), the container will be located in a minimal Hyper-V based [virtual
environment](https://docs.microsoft.com/en-us/windows-server/virtualization/hyper-v/hyper-v-technology-overview).
For a more complete overview about Docker container paths, see
[here](https://www.freecodecamp.org/news/where-are-docker-images-stored-docker-container-paths-explained/).
This means that you really will not be able to access any files located in the Docker container from
your host machine. Instead you must use VSCode or another tool to connect to the container and edit
files through that means.
