All operations performed with 10x10 matrices stored in static memory, release build

_**NOTE:** modm matrix operations perform significantly worse during debug build due to copy
semantics used that are otherwise optimized during the release build._

_**NOTE:** While CMSIS can handle matrix operations > 10x10, since modm does stack copies of
matrices, the embedded system crashes when working with matrix operations that are too large because
of stack smashing._

## Matrix multiplication
- CMSIS: 64 us
- MODM: 69 us
## Matrix addition
- CMSIS: 6
- MODM: 22
## Matrix inverse
- CMSIS: 149
- MODM: doesn't have inverse support
## Matrix transpose
- CMSIS: 6
- MODM: 20
