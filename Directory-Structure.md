This is a brief guide on the directory layout of `aruwlib`.

## .`gitlab`

Contains the [CI](Definitions#continuous-integration-ci) Docker image and build tools.

## `.vscode`

Contains VSCode project settings and tasks for building code through the VSCode interface.

## `build_tools`

Contains tooling used to build generated code. Files in this directory may be generated and used in
projects that utilize aruwlib.

## `docs`

Contains all the relevant information necessary for building the generated documentation using
[Exhale](https://exhale.readthedocs.io/en/latest/).

## `modm`

A submodule (embedded git repository) of [`modm`](https://github.com/modm-io/modm), used during code
generation.

## `scripts`

Contains python and shell scripts that are either useful for developers or are used during CI
builds.

## `src`

Contains all header and source files that are a part of aruwlib.

## `test`

Contains all tests for aruwlib that validate the correctness of the library code.

## `test-project`

Contains a test project that aruwlib. It can be used for basic testing and as a generation/build
smoke-test. However, changes made by ARUW members should typically be tested in the context of
aruw-mcb before being merged here.
