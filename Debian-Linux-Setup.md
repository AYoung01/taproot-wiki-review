This guide is designed for Ubuntu 18.04 and 20.04 LTS. With this setup, full unit testing
functionality and deploying to the MCB functionality is supported.

__**Note:** this guide assumes your are cloning and working with the aruwlib repository but the
guide is generic enough to be used with [aruw-mcb](https://gitlab.com/aruw/controls/aruw-mcb),
[aruwlib-template-project](https://gitlab.com/aruw/controls/aruwlib-template-project),
[aruwlib-examples](https://gitlab.com/aruw/controls/aruwlib-examples), or
[aruw-edu](https://gitlab.com/aruw/controls/aruw-edu). To use this guide with any of these projects,
clone the respective repository instead of aruwlib.__

1. Run the following commands in a terminal:
    ```
    sudo apt-get install python3 python3-pip scons git
    sudo apt-get --no-install-recommends install doxygen
    pip3 install modm
    sudo apt-get install gcc-avr binutils-avr avr-libc avrdude
    sudo apt-get install openocd
    pip3 install gdbgui
    sudo apt-get install gcc build-essential libboost-all-dev
    pip3 install lbuild
    pip3 install pyelftools
    ```
2. Add lbuild to your path:
    - Open `~/.bashrc` with a text editor.
    - Scroll down to the end of the file and type this:
        ```
        PATH="$HOME/.local/bin:$PATH"
        export PATH
        ```
    - Save the new updates.
3. Download the [latest
   version](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads)
   of the arm-none-eabi toolchain. In particular, download
   "gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2", and unzip the file (`tar -xvf
   gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2`).
4. Add the `/bin` folder of `gcc-arm-none-eabi-9-2020-q2-update` to your path:
    - Open `~/.bashrc` with a text editor.
    - Scroll down to the end of the file, and right above `PATH="$HOME/.local/bin:$PATH"`, type
      this:
        ```
        PATH=path/to/gcc-arm-none-eabi-9-2020-q2-update/bin:$PATH
        ```
       Where `path/to/gcc-arm-none-eabi-9-2020-q2-update` is replaced with your own path to the
       toolchain.

14. When running on Ubuntu 18.04 LTS, an additional step must be taken to insure python 3 is used.
    If you are running 18.04, for a less intrusive way to run all scons scripts with Python 3 add
    this to your `.bashrc` or `.zshrc`:
    ```
    alias scons="/usr/bin/env python3 $(which scons)"
    ```
15. Download and compile the latest version of openocd (`apt-get install openocd` is not enough by
    itself).
    - In particular, you must clone the [openocd git repo](https://github.com/ntfreak/openocd), then
      run the following commands in a terminal located at the root directory of the cloned repo
      (taken from the openocd [README](https://github.com/ntfreak/openocd/blob/master/README)):
        ```
        ./bootstrap
        ./configure
        make
        sudo make  install
        ```
16. Install the ST-Link V2 driver. This is done through the terminal. The following instructions
    were taken and partially modified from [this
    site](https://freeelectron.ro/installing-st-link-v2-to-flash-stm32-targets-on-linux/).
    ```
    sudo apt-get install git make cmake libusb-1.0-0-dev
    sudo apt-get install gcc build-essential
    cd
    mkdir stm32
    cd stm32
    git clone https://github.com/texane/stlink
    cd stlink
    cmake .
    make
    sudo make install
    cd bin
    sudo cp st-* /usr/local/bin
    cd ..
    sudo cp config/udev/rules.d/49-stlinkv* /etc/udev/rules.d/ 
    sudo udevadm control --reload
    ```
17. If you have an ST-Link on hand (if you are in lab, ask someone to help find one), test if the
    installation worked correctly. See the end of the document mentioned in the previous step to
    insure the ST-Link driver is properly installed.
18. Download and install [VSCode](https://code.visualstudio.com/download).
19. Clone this repository: In a new terminal (Git Bash, anaconda, or cmd should work) type `git
    clone --recursive https://gitlab.com/aruw/controls/aruwlib.git`.
    - _If you forget the `--recursive`, run: `git submodule update --init --recursive`._
20. Restart your computer to insure all installs have been updated.

## VSCode setup guide

1. Open the `aruwlib` folder in VSCode. The first time you try to open this folder, you can start
   VSCode, then in the "Welcome" page, under the "Start" menu, find "Open folder...".
2. When you open up the folder, VSCode should prompt you to install the recommended extensions.
   Click "install recommended extensions". You should now have the "C/C++", "Cortex-Debug", and
   "aruw robot chooser" extensions.
    - If you do not, in extensions: marketplace (to open, type
      <kbd>Ctrl</kbd>+<kbd>shift</kbd>+<kbd>X</kbd>), search "@recommended" and install all under
      "WORKSPACE RECOMMENDED".
