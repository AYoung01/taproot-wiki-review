Projects using Taproot do not import the code in this repo directly. Instead, the provided library
code is preprocessed by `lbuild`, a code templating tool.

User projects include the `taproot` repository as a git "submodule" -- a git repo within a git repo.
They then define a `project.xml` which configures Taproot with chosen hardware parameters and
requirements. From within the same directory, running `lbuild build` will re-generate all requested
code.

Edits to generated code should be avoided: they will be overwritten the next time `lbuild` is run.
Prefer contributing changes to the upstream Taproot project, or forking the library and pointing
your submodule at this new fork. As a last resort, code edits can be made, but will increase
maintenance overhead. 

The [template project](https://gitlab.com/aruw/controls/aruwlib-template-project) provides an
example of this configuration in practice.

## Diagram

<!-- ARUW members: source is this presentation -- https://docs.google.com/presentation/d/1lwIkYHm3XM6PEP7P3M8ILbSRlELJpB7PG2VyIfYaZXo/edit#slide=id.ge60228f307_0_36 -->

<img src="uploads/taproot_generation_overview.png" alt="lbuild converting the lbuild submodule into generated code, to be used by user code" width=700>
