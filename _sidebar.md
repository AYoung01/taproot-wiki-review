**[Home](Home)**

**Architecture Design**

- [Directory Structure](Directory-Structure)
- [Build Targets Overview](Build-Targets-Overview)
- [Drivers Architecture](Drivers-Architecture)
- [Command Subsystem Framework](Command-Subsystem-Framework)
- [Code Generation in User Projects](Code-Generation-in-User-Projects)
- [Generated Documentation](https://aruw.gitlab.io/controls/taproot/)

**Software Tools**

- [Docker Overview](Docker-Overview)
- [Debugging Safety Information](Debugging-Safety-Information)
- [Debugging With ST-Link](Debugging-With-STLink)
- [Debugging With J-Link](Debugging-With-JLink)
- [Git Tutorial](Git-Tutorial)
- [How to Chip Erase the MCB](How-to-Chip-Erase-the-MCB)
- [RoboMaster Server Setup](RoboMaster-Server-Setup)

**Software Profiling**

- [Profiling Main](Profiling-Main)
- [Profiling Matrix Operations](Profiling-Matrix-Operations)

**Full Setup Guides**

- [Windows Setup](Windows-Setup)
- [Debian Linux Setup](Debian-Linux-Setup)
- [Windows WSL Setup](Windows-WSL-Setup)
- [Docker Container Setup](Docker-Container-Setup)

**Miscellaneous and Brainstorming**

- [Style Guide](C++-ARUW-Style-Guide)
- [Unit Test Guide](Unit-Test-Guide)
- [Definitions](Definitions)

_Submit edits to this wiki via the [taproot-wiki-review repo](https://gitlab.com/aruw/controls/taproot-wiki-review)._
