These tests were carried out using `robot=TARGET_SOLDIER`, on the new soldier with all motors
connected, running default commands.

## Profiling vanilla develop per 7/5/2020

### Code run every two milliseconds

```cpp
Drivers::mpu6500.read();
Drivers::errorController.update();
Drivers::commandScheduler.run();
Drivers::djiMotorTxHandler.processCanSendData();
```

- Debug build
    - 309 microseconds to execute with no hardware connected.
        - `Drivers::mpu6500.read()`: 228 microseconds.
        - `Drivers::errorController.update()`: 10 microseconds.
        - `Drivers::commandScheduler.run()`: 62 microseconds (two `SystemErrors` are being added to
          the scheduler each iteration, which take a significant time).
        - `Drivers::djiMotorTxHandler.processCanSendData()`: 9 microseconds.

- Release build
    - 234 microseconds to execute with no hardware connected.
        - `Drivers::mpu6500.read()`: 178 microseconds.
        - `Drivers::errorController.update()`: 4 microseconds.
        - `Drivers::commandScheduler.run()`: 47 microseconds (two `SystemErrors` are being added to
          the scheduler each iteration, which take a significant time).
        - `Drivers::djiMotorTxHandler.processCanSendData()`: 5 microseconds.

### IO update

```cpp
Drivers::canRxHandler.pollCanData();
Drivers::xavierSerial.updateSerial();
Drivers::refSerial.updateSerial();
Drivers::remote.read();
```

- Debug build: takes 8 microseconds to execute with no hardware connected.

- Release build: takes 5 microseconds to execute with no hardware connected.

## Profiling the updated drivers architecture per 7/5/2020

### Code run every two milliseconds

```cpp
getDrivers().mpu6500.read();
getDrivers().errorController.update();
getDrivers().commandScheduler.run();
getDrivers().djiMotorTxHandler.processCanSendData();
```

- Debug build
    - 307 microseconds to execute with no hardware connected.
        - `getDrivers().mpu6500.read()`: 227 microseconds.
        - `getDrivers().errorController.update()`: 10 microseconds.
        - `getDrivers().commandScheduler.run()`: 62 microseconds.
        - `getDrivers().djiMotorTxHandler.processCanSendData()`: 9 microseconds.

- Release build
    - 232 microseconds to execute with no hardware connected.
        - `getDrivers().mpu6500.read()`: 177 microseconds
        - `getDrivers().errorController.update()`: 3 microseconds.
        - `getDrivers().commandScheduler.run()`: 47 microseconds (two `SystemErrors` are being added
          to the scheduler each iteration, which take a significant time).
        - `getDrivers().djiMotorTxHandler.processCanSendData()`: 5 microseconds

### IO update

```cpp
getDrivers().canRxHandler.pollCanData();
getDrivers().xavierSerial.updateSerial();
getDrivers().refSerial.updateSerial();
getDrivers().remote.read();
```

- Debug build: takes 7 microseconds to execute with no hardware connected.

- Release build: takes 5 microseconds to execute with no hardware connected.