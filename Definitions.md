## Source control organization

### VSCode project folder 

The root folder of the repository you are working with in VSCode. When you open up VSCode (to open,
you can simply search for "VSCode" in the start menu), under "Start," select "Open folder...," then
find the root directory of the repository you want to open. For example, suppose you have cloned
aruwlib and want to open the project folder, you should find and open the folder `aruwlib`, and not
any other folder.

### `aruwlib`

**This code is reused from year to year and is not specific to any particular robot.**

Our main library containing code that handles the following:
- Architecture: includes things such as timers and clocks.
- Communication: includes GPIO related IO as well code for handling protocols such as
  [CAN](#controller-area-network-can).
- Debugging: includes an error logger that stores and displays errors using LED bit patterns on the
  MCB.
- Platform: includes, for example, the command scheduler and related components.
- Algorithms: includes some miscellaneous math utilities as well as various algorithms.
- Other robot-generic utils: various utilities that can be reused from year to year that doesn't
  fall into one of the above categories.

## Hardware related protocols

### **C**ontroller **A**rea **N**etwork (**CAN**)

A pair of wire which connect each motor controller to the MCB, so they can talk back and forth.

## Hardware

### Motor controller

An electronic board which we purchase together with the motors themselves. They take commands via
CAN, and in turn send power to the motors. They also report motor speed and position back to the MCB
via CAN. Motor controllers are identified by their unique ID.

### **M**ain **C**ontrol **B**oard (**MCB**)

This board runs the custom firmware which we write. It receives input from sensors, the remote, and
other auxiliary devices, and controls motors and actuators.

<img src="./uploads/robomaster_type_a_board.PNG" alt="mcb" height=300px>

### Remote

The remote is the main way to control the robot while it is running. The remote connects to a
receiver that is connected to the MCB. There are two joysticks and two switches on the remote itself
(see the image below). Plugging the robot into a computer with the RoboMaster client running with
the RoboMaster remote driver installed allows for mouse and keyboard controls to be piped through
the remote and to the receiver and subsequently the MCB.

<img src="./uploads/remote_image.jpg" alt="remote" height=300px>

### modm
> modm (pronounced like dial-up "modem") is a toolbox for building custom C++17 libraries tailored
to your embedded device. modm generates startup code, HALs and their implementations, communication
protocols, drivers for external devices, BSPs, etc… in a modular, customizable process that you can
fine-tune to your needs. [[1]](modm.io)

This is the way we interface with hardware.

### General-purpose input/output pin (GPIO)

> A general-purpose input/output pin is an uncommitted digital signal pin on an integrated circuit
> or electronic circuit board whose behavior---including whether it acts as input or output---is
> controllable by the user at run time
> [[2]](https://en.wikipedia.org/wiki/General-purpose_input/output).

### Bit Banging

> Slang for any method of data transmission that employs software as a substitute for dedicated
> hardware to generate transmitted signals or process received signals"
> [[3]](https://en.wikipedia.org/wiki/Bit_banging).

### Hardware abstraction layer (HAL)

A layer of programming that allows a program to interact with a hardware device at a general or
abstract level rather than at a detailed hardware level.

### Board support package

A set of libraries offering a hardware abstraction layer (HAL) to a higher level operating system
(in our case the main programming running on the embedded device).

## General Terms

### Unit tests

A test designed to check the functionality of a single, isolated component. Typically they are self
contained and require few inputs and outputs.

### Hosted Target

The term "hosted" specifically refers to the operating system on which the code is built. For
instance, if you build on Linux, you can replace "hosted" with "Linux". "Running code on the hosted
target" simply means running code on the operating system from which the code was built. Conversely,
the MCB is a hardware target without an operating system, and is thus not a "hosted" environment.

### Continuous Integration (CI)

A practice where developers merge code changes frequently, during which automated builds and tests
are run.
