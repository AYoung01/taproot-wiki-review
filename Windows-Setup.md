This setup is depreciated because our unit testing environment must run on a Linux based OS.
However, this guide is still complete and will allow you to develop and deploy to the MCB.

__**Note:** this guide assumes your are cloning and working with the aruwlib repository but the
guide is generic enough to be used with [aruw-mcb](https://gitlab.com/aruw/controls/aruw-mcb),
[aruwlib-template-project](https://gitlab.com/aruw/controls/aruwlib-template-project),
[aruwlib-examples](https://gitlab.com/aruw/controls/aruwlib-examples), or
[aruw-edu](https://gitlab.com/aruw/controls/aruw-edu). To use this guide with any of these projects,
clone the respective repository instead of aruwlib.__

1. Download
   [arm-gcc-toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads).
   When you open downloads page, you will see a number of installers. Choose the .zip version of
   newest toolchain release. Extract the contents of the downloaded file to a permanent location of
   your choice.
2. Download the latest version of [openocd](https://gnutoolchains.com/arm-eabi/openocd/). Download
   the newest version of the software and extract the files to a permanent location. The downloaded
   file is of type `.7zip`, so you will need a way to extract 7zip files, either by installing the
   7zip extractor or by using an online unzipper.
3. Download and install [anaconda](https://www.anaconda.com/distribution/). During installation of
   Anaconda, make sure to check the 'Add to PATH' box.
4. Add the path of the `/bin` directories of OpenOCD and the GNU Toolchain to your PATH environment
   variable.
    - In your Start menu, search for "environment" and select "Edit the system environment
      variables."

    - In the "User variables for _{your username here}_", where _{your username here}_ is a
      placeholder for your actual name. Double click on "Path."<br><br>
      <img
      src="https://gitlab.com/aruw/code-2019-2020/aruw-mcb-2019-2020/uploads/49592ade19d3a60bb0377d1ae54fd3e6/EnvironmentVarsMain.jpg"
      height="500px">

    - In "Edit environment variable," click "New."<br><br>
      <img
      src="https://gitlab.com/aruw/code-2019-2020/aruw-mcb-2019-2020/uploads/8979edf979083ace722dba00883ad153/EnvironmentVarsPathVar.jpg"
      height="500px">

    - Add the absolute path of the `/bin` directory of one of the tools listed above to the box that
      shows up after you click "New". Repeat this process for all of the necessary paths listed
      above.<br><br>
      <img
      src="https://gitlab.com/aruw/code-2019-2020/aruw-mcb-2019-2020/uploads/171f2c159ee4a765680aa7222627a7ec/EnvironmentVarsPathAddition.jpg"
      height="500px">

    - Select "OK."
5. Download the ST-Link V-2 driver
   [here](https://drive.google.com/drive/u/1/folders/1Ndk8Q-uUtzo3sQtzOoguDAVBZSM3IKT1), unzip, and
   run the executable to install the driver.
6. Download
   [ozone_jlink_jscope.zip](https://drive.google.com/drive/u/1/folders/1E9i1JBILotoFClKc6L3-m4OWuW0kiO5A)
   in our google drive. This contains installers for Ozone, the J-Link drivers, and J-Scope (all
   J-Link related software). Once downloaded and unzipped, run the three installers.
7. Open anaconda prompt (type "Anaconda Prompt" into the start menu to start the prompt) and run the
   following commands: <br>
    ```
    conda create --name modm python=3 pip
    activate modm
    conda install -c conda-forge git pywin32
    pip install jinja2 scons future pyelftools lbuild
    ```
8. Download and install [VSCode](https://code.visualstudio.com/download).
9. Clone this repository: In a new terminal (Git Bash, anaconda, or cmd should work) type `git clone
   --recursive https://gitlab.com/aruw/controls/aruw-mcb.git`.
    - _If you forget the `--recursive`, run: `git submodule update --init --recursive`._
10. Restart your computer to insure all installs have been updated.

## VSCode setup guide

1. Open the `aruwlib` folder in VSCode. The first time you try to open this folder, you can start
   VSCode, then in the "Welcome" page, under the "Start" menu, find "Open folder...". 
2. When you open up the folder, VSCode should prompt you to install the recommended extensions.
   Click "install recommended extensions". You should now have the "C/C++", "Cortex-Debug", and
   "aruw robot chooser" extensions.
    - If you do not, in extensions: marketplace (to open, type
      <kbd>Ctrl</kbd>+<kbd>shift</kbd>+<kbd>X</kbd>), search "@recommended" and install all under
      "WORKSPACE RECOMMENDED".
3. For VSCode intellisense to work properly, you need to add your compiler path to the .vscode
   project folder. To do so, type <kbd>ctrl</kbd>+<kbd>P</kbd>, type `>c/c++ edit configurations
   (JSON)`, and hit enter. This will generate a `c_cpp_properties.json` file with some default
   settings. Replace the contents of this file with the below json. Edit the "compilerPath" setting
   so it matches the example below. In particular, the "compilerPath" option should be the absolute
   path name to your version of `arm-none-eabi-gcc.exe` that you just installed above, which will be
   located in the arm-gcc-toolchain's `/bin` folder.
```json
{
    "configurations": [
        {
            "name": "Win32",
            "compilerPath": "C:/path/to/bin/arm-none-eabi-gcc.exe"
        }
    ],
    "version": 4
}
```
